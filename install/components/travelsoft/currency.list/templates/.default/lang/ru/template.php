<?php
$MESS['UF_USD'] = 'Доллар США';
$MESS['UF_EUR'] = 'Евро';
$MESS['UF_RUB'] = 'Российский рубль';
$MESS['UF_UAH'] = 'Гривна';
$MESS['DATE'] = 'Дата';
$MESS['SOURCE'] = 'Источник';
$MESS['CBRB'] = 'ЦБ РБ';
