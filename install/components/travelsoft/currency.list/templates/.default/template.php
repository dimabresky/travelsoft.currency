<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if (!empty($arResult['ERRORS'])):?>
    <!--TODO: show error-->
<? else: ?>
    <div class="widjet__currency currency">
        <? foreach ($arResult['DISPLAY_CURRENCIES'] as $displayCurrency): ?>
            <div class="currency__item <?= $displayCurrency['lower_field']; ?>">
                <div class="row">
                    <div class="col-md-6">
                        <?= GetMessage($displayCurrency['uf_field']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $arResult['CURRENT_COURSES'][$displayCurrency['uf_field']]; ?> &nbsp;
                        <span class="<?= $arResult['DIFF_COURSES'][$displayCurrency['uf_field']] < 0 ? 'red' : 'green'; ?>">
                            <?= $arResult['DIFF_COURSES'][$displayCurrency['uf_field']]; ?>
                        </span>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
        <div class="currency__item">
            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="currency__date">
                        <?= GetMessage('DATE') ?>: <strong><?= $arResult['DATE_LAST_UPDATE']; ?></strong>
                    </div>
                    <div class="currency__source">
                        <?= GetMessage('SOURCE') ?>:
                        <strong><?= GetMessage('CBRB') ?></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>
